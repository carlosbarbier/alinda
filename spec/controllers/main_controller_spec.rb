require "rails_helper"

RSpec.describe MainController, :type => :controller do
  it { should route(:get, '/').to(action: :index) }
  it { should route(:get, '/about').to(action: :about) }
  it { should route(:get, '/contact').to(action: :contact) }
  it { should route(:get, '/how-it-works').to(action: :how) }
end

