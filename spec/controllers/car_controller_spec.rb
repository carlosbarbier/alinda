require "rails_helper"

RSpec.describe(CarController, :type => :controller) do
  it {
    should route(:get, '/company/clients/1/car/register').to(action: :register, id: 1)
  }
end
