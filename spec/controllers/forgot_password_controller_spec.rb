require 'rails_helper'

RSpec.describe ForgotPasswordController, :type => :controller do
  it { should route(:get, '/employee/forgot-password').to(action: :edit) }
end

