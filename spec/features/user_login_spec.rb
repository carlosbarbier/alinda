require 'rails_helper'

RSpec.feature 'User Login Page', type: :feature do
  it 'visit page' do
    visit 'employee/login'
    expect(page).to have_content 'Login'
  end
  it 'can enter a email' do
    visit 'employee/login'
    fill_in :email, with: 'karl@gmail.com'
    click_on 'Login'
    expect(page).to have_content 'Invalid email or password'
  end
  it 'can enter a password' do
    visit 'employee/login'
    fill_in :password, with: '123456'
    click_on 'Login'
    expect(page).to have_content 'Invalid email or password'
  end
end
