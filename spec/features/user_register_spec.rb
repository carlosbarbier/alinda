require 'rails_helper'

RSpec.feature 'User Register Page', type: :feature do
  subject { page }
  before do
    visit employee_register_path
  end
  it 'verify that page has user verification ' do
    expect(page).to have_content 'User Registration'
  end

  it 'can enter a email' do
    fill_in :email, with: 'karl@gmail.com'
    click_on 'Register'
    expect(page).to have_content 'Invalid employee code'
  end
  it 'can enter a password' do
    fill_in :password, with: '123456'
    click_on 'Register'
    expect(page).to have_content 'Invalid employee code'
  end
  it 'can enter a code' do
    fill_in :code, with: '123456'
    click_on 'Register'
    expect(page).to have_content 'Invalid employee code'
  end

  it 'can enter a code,email,password,confirmation_password,name' do
    fill_in :code, with: '123456'
    fill_in :name, with: 'Paul'
    fill_in :password, with: '123456'
    fill_in :password_confirmation, with: '123456'
    fill_in :email, with: 'paul@gmail.com'
    click_on 'Register'
    expect(page).to have_content 'Invalid employee code'
  end
end
