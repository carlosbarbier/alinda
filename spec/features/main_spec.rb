require 'rails_helper'

RSpec.feature 'Home page', type: :feature do
  subject { page }
  before do
    visit root_path
  end

  it 'visit page verify page has Register now ' do
    expect(page).to have_content 'Register Now'
  end

  it 'visit page verify page has Business ' do
    expect(page).to have_content 'You take care of your Business'
  end
  it 'visit page verify page has System' do
    expect(page).to have_content 'We take care of your System'
  end

  it 'will redirect to company register page' do
    visit root_path
    click_link(href: '/company/register')
    expect(page.current_path).to eq company_register_path
  end

end
