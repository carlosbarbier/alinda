require 'spec_helper'

RSpec.describe CompanyController, :type => :controller do
  it { should route(:get, '/company/register').to(action: :create) }
end