require 'rails_helper'

RSpec.describe Client, :type => :model do
  before{
    @company = Company.create({ address: 'Baggot Street', email: 'alind@gmail.com', name: 'Alinda', phone: '353899424479', code: '123456', country: 'Ireland', created_at: '2021-03-10 13:19:16.236753', updated_at: '2021-03-10 13:19:16.236753', })
    @user=User.create({password: '123456', email: 'karl@gmail.com', name: 'karl', password_confirmation: '123456', code: '123456', created_at: '2021-03-10 13:19:16.236753', updated_at: '2021-03-10 13:19:16.236753', token: 'token', company_id: @company.id, is_active: false})
  }
  subject {
    described_class.new(email: 'karl@gmail.com', first_name: 'karl', last_name: 'karl', address: 'address', created_at: '2021-03-10 13:19:16.236753', updated_at: '2021-03-10 13:19:16.236753', licence: 'license', user_id: @user.id, phone: "+353899424470",dob:"17/02/1991")
  }
  describe 'Associations' do
    it { should have_many(:cars) }
  end

  describe 'Validations' do
    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_presence_of(:address) }
    it { is_expected.to validate_presence_of(:licence) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:phone) }
    it { is_expected.to validate_presence_of(:dob) }
    it 'is not valid with an invalid email' do
      subject.email = "nil"
      expect(subject).to_not be_valid
    end
    it 'is not valid with an invalid dob' do
      subject.email = "nil"
      expect(subject).to_not be_valid
    end
    it 'is not valid with an invalid phone' do
      subject.email = "nil"
      expect(subject).to_not be_valid
    end
  end

  describe 'Column Specification' do
    it { should have_db_column(:first_name).of_type(:string)}
    it { should have_db_column(:last_name).of_type(:string)}
    it { should have_db_column(:address).of_type(:string)}
    it { should have_db_column(:licence).of_type(:string)}
    it { should have_db_column(:email).of_type(:string)}
    it { should have_db_column(:phone).of_type(:string)}
    it { should have_db_column(:dob).of_type(:string)}
    it { should have_db_column(:user_id).of_type(:integer)}
    it { should have_db_column(:created_at).of_type(:datetime) }
    it { should have_db_column(:updated_at).of_type(:datetime) }
  end
end
