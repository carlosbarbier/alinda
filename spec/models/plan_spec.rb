require 'rails_helper'

RSpec.describe Plan, :type => :model do

  subject {
    described_class.new(
      name: 'New Plan',
      description: 'plan description',
      price: '100',
      stripe_id: 'stripe_id',
      created_at: '2021-03-10 13:19:16.236753',
      updated_at: '2021-03-10 13:19:16.236753'
    )
  }
  describe 'Associations' do
    it { should have_many(:cars) }
  end

  describe 'Validations' do
    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.to validate_presence_of(:stripe_id) }
    it { is_expected.to validate_presence_of(:price) }
  end

  describe 'Column Specification' do
    it { should have_db_column(:name).of_type(:string)}
    it { should have_db_column(:description).of_type(:string)}
    it { should have_db_column(:price).of_type(:string)}
    it { should have_db_column(:stripe_id).of_type(:string)}
    it { should have_db_column(:created_at).of_type(:datetime) }
    it { should have_db_column(:updated_at).of_type(:datetime) }
  end
end
