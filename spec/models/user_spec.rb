require 'rails_helper'

RSpec.describe User, :type => :model do
  before { @company = Company.create({ address: 'Baggot Street', email: 'alind@gmail.com', name: 'Alinda', phone: '353899424479', code: '123456', country: 'Ireland', created_at: '2021-03-10 13:19:16.236753', updated_at: '2021-03-10 13:19:16.236753', }) }
  subject {
    described_class.new(password: '123456', email: 'karl@gmail.com', name: 'karl', password_confirmation: '123456', code: '123456', created_at: '2021-03-10 13:19:16.236753', updated_at: '2021-03-10 13:19:16.236753', token: 'token', company_id: @company.id, is_active: false)
  }
  describe 'Associations' do
    it { should belong_to(:company)}
  end
  describe 'Validations' do
    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to have_secure_password }
    it { is_expected.to validate_presence_of(:code) }
    it 'is not valid with an invalid email' do
      subject.email = "nil"
      expect(subject).to_not be_valid
    end
  end

  describe 'Column Specification' do
    it { should have_db_column(:email).of_type(:string) }
    it { should have_db_column(:name).of_type(:string) }
    it { should have_db_column(:code).of_type(:string) }
    it { should have_db_column(:token).of_type(:string) }
    it { should have_db_column(:is_active).of_type(:boolean) }
    it { should have_db_column(:company_id).of_type(:integer) }
    it { should have_db_column(:company_id).of_type(:integer) }
    it { should have_db_column(:created_at).of_type(:datetime) }
    it { should have_db_column(:updated_at).of_type(:datetime) }
  end
end
