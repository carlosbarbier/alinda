require 'rails_helper'

RSpec.describe Company, :type => :model do

  subject {
    described_class.new(
      address: 'Baggot Street',
      email: 'alind@gmail.com',
      name: 'Alinda',
      phone: '353899424479',
      code: '123456',
      country: 'Ireland',
      created_at: '2021-03-10 13:19:16.236753',
      updated_at: '2021-03-10 13:19:16.236753',
    )
  }
  describe 'Associations' do
    it { should have_many(:user) }
    it { should have_many(:clients) }
    it { should have_many(:cars) }
    it { should have_many(:accidents) }
  end

  describe 'Validations' do
    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end
    it 'is valid without id' do
      subject.id = nil
      expect(subject).to be_valid
    end
    it 'is valid without a code' do
      subject.code = nil
      expect(subject).to be_valid
    end
    it 'is not valid without a email' do
      subject.email = nil
      expect(subject).to_not be_valid
    end
    it 'is not valid with invalid email' do
      subject.email = 'yyy'
      expect(subject).to_not be_valid
    end
    it 'is not valid with invalid phone number' do
      subject.phone = 'yyy'
      expect(subject).to_not be_valid
    end

    it 'is not valid without a phone' do
      subject.phone = nil
      expect(subject).to_not be_valid
    end

    it 'is not valid without a country' do
      subject.country = nil
      expect(subject).to_not be_valid
    end

    it 'is not valid without a address' do
      subject.address = nil
      expect(subject).to_not be_valid
    end
  end

  describe 'Column Specification' do
    it { should have_db_column(:email).of_type(:string)}
    it { should have_db_column(:name).of_type(:string)}
    it { should have_db_column(:address).of_type(:string)}
    it { should have_db_column(:phone).of_type(:string)}
    it { should have_db_column(:code).of_type(:string)}
    it { should have_db_column(:country).of_type(:string)}
    it { should have_db_column(:created_at).of_type(:datetime) }
    it { should have_db_column(:updated_at).of_type(:datetime) }
  end
end
