require 'rails_helper'

RSpec.describe Contact, type: :model do
  subject {
    described_class.new(name: 'Karl max', email: 'alind@gmail.com', subject: 'subject', message: 'message', created_at: '2021-03-10 13:19:16.236753', updated_at: '2021-03-10 13:19:16.236753', )
  }
  
  describe 'Validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:subject) }
    it { is_expected.to validate_presence_of(:message) }
    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end
    it 'is not valid with an invalid email' do
      subject.email = 'invalid'
      expect(subject).to_not be_valid
    end
  end

  describe 'Column Specification' do
    it { should have_db_column(:name).of_type(:string) }
    it { should have_db_column(:email).of_type(:string) }
    it { should have_db_column(:subject).of_type(:string) }
    it { should have_db_column(:message).of_type(:string) }
    it { should have_db_column(:created_at).of_type(:datetime) }
    it { should have_db_column(:updated_at).of_type(:datetime) }
  end
end
