require 'rails_helper'

RSpec.describe Subscription, type: :model do
  before {
    @company = Company.create({ address: 'Baggot Street', email: 'alind@gmail.com', name: 'Alinda', phone: '353899424479', code: '123456', country: 'Ireland', created_at: '2021-03-10 13:19:16.236753', updated_at: '2021-03-10 13:19:16.236753', })
    @user = User.create({ password: '123456', email: 'karl@gmail.com', name: 'karl', password_confirmation: '123456', code: '123456', created_at: '2021-03-10 13:19:16.236753', updated_at: '2021-03-10 13:19:16.236753', token: 'token', company_id: @company.id, is_active: false })
    @plan = Plan.create({ name: 'New Plan', description: 'plan description', price: '100', stripe_id: 'stripe_id', created_at: '2021-03-10 13:19:16.236753', updated_at: '2021-03-10 13:19:16.236753' })
    @client = Client.create({ email: 'karl@gmail.com', first_name: 'karl', last_name: 'karl', address: 'address', created_at: '2021-03-10 13:19:16.236753', updated_at: '2021-03-10 13:19:16.236753', licence: 'license', user_id: @user.id, phone: "+353899424470", dob: "17/02/1991" })
    @car = Car.create({ make: 'Audit', year: '2020', color: 'red', fuel: 'petrol', engine: '15l', door: 4, tax: 'May-2020', transmission: 'Automatic', mileage: '10000', user_id: @user.id, plan_id: @plan.id, client_id: @client.id, created_at: '2021-03-10 13:19:16.236753', updated_at: '2021-03-10 13:19:16.236753', })
  }
  subject {
    described_class.new(stripe_id: 'stripe_id', is_cancelled: true, car_id: @car.id, created_at: '2021-03-10 13:19:16.236753', updated_at: '2021-03-10 13:19:16.236753',)
  }
  describe 'Associations' do
    it { should belong_to(:car) }
  end

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:stripe_id) }
    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end
  end

  describe 'Column Specification' do
    it { should have_db_column(:stripe_id).of_type(:string) }
    it { should have_db_column(:id).of_type(:integer) }
    it { should have_db_column(:car_id).of_type(:integer) }
    it { should have_db_column(:is_cancelled).of_type(:boolean) }
    it { should have_db_column(:created_at).of_type(:datetime) }
    it { should have_db_column(:updated_at).of_type(:datetime) }
  end
end
