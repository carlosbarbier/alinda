ActiveRecord::Migration.maintain_test_schema!
ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'rails-controller-testing'
require 'capybara/rails'
require 'capybara/rspec'


class ActiveSupport::TestCase
  Rails::Controller::Testing.install
  parallelize(workers: :number_of_processors, with: :threads)
  fixtures :all
end
