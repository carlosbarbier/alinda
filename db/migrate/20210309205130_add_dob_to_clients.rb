class AddDobToClients < ActiveRecord::Migration[6.0]
  def change
    add_column :clients, :dob, :string
  end
end
