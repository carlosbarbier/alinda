class CreateCars < ActiveRecord::Migration[6.0]
  def change
    create_table :cars do |t|
      t.string :make
      t.string :year
      t.string :color
      t.string :fuel
      t.string :engine
      t.string :door
      t.string :tax
      t.string :transmission
      t.string :mileage
      t.integer :user_id
      t.integer :plan_id
      t.integer :client_id
      t.timestamps
    end
    add_foreign_key :cars, :users
    add_foreign_key :cars, :plans
    add_foreign_key :cars, :clients
  end
end
