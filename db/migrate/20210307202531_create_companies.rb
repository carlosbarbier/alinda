
class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :address
      t.string :code
      t.string :email
      t.string :phone
      t.timestamps
    end
    add_index :companies, :email, unique: true
    add_index :companies, :phone, unique: true
    add_index :companies, :code, unique: true
  end
end
