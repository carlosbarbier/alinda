class AddIsCancelledToSubsciptions < ActiveRecord::Migration[6.0]
  def change
    add_column :subscriptions, :is_cancelled, :boolean,default: false
  end
end
