class CreateSubscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :subscriptions do |t|
      t.string :stripe_id
      t.integer :car_id
      t.timestamps
    end
  end
end
