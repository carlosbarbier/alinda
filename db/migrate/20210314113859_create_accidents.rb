class CreateAccidents < ActiveRecord::Migration[6.0]
  def change
    create_table :accidents do |t|
      t.string :date
      t.string :time
      t.string :location
      t.string :description
      t.integer :user_id
      t.integer :car_id
      t.timestamps
    end
    add_foreign_key :accidents, :users
    add_foreign_key :accidents, :cars
    end
end
