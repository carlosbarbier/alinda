# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_04_04_180400) do

  create_table "accidents", force: :cascade do |t|
    t.string "date"
    t.string "time"
    t.string "location"
    t.string "description"
    t.integer "user_id"
    t.integer "car_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "cars", force: :cascade do |t|
    t.string "make"
    t.string "year"
    t.string "color"
    t.string "fuel"
    t.string "engine"
    t.string "door"
    t.string "tax"
    t.string "transmission"
    t.string "mileage"
    t.integer "user_id"
    t.integer "plan_id"
    t.integer "client_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "clients", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "address"
    t.string "licence"
    t.string "email"
    t.string "phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "dob"
    t.integer "user_id"
    t.index ["email"], name: "index_clients_on_email", unique: true
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "code"
    t.string "email"
    t.string "phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "country"
    t.index ["code"], name: "index_companies_on_code", unique: true
    t.index ["email"], name: "index_companies_on_email", unique: true
    t.index ["phone"], name: "index_companies_on_phone", unique: true
  end

  create_table "contacts", force: :cascade do |t|
    t.string "email"
    t.string "subject"
    t.string "message"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "plans", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "stripe_id"
  end

  create_table "subscriptions", force: :cascade do |t|
    t.string "stripe_id"
    t.integer "car_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "is_cancelled", default: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "company_id"
    t.string "token"
    t.boolean "is_active"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "accidents", "cars"
  add_foreign_key "accidents", "users"
  add_foreign_key "cars", "clients"
  add_foreign_key "cars", "plans"
  add_foreign_key "cars", "users"
  add_foreign_key "clients", "users"
  add_foreign_key "users", "companies"
end
