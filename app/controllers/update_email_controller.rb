class UpdateEmailController < ApplicationController
  before_action :require_user_logged_in!
  before_action :get_user, only: [:edit, :update]
  def edit
  end

  def update

    if params[:email].empty? || !params[:email].to_s.match((/\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i))
      flash.now[:alert] = 'Invalid email'
      render :edit
    else
      @user.update_column(:email, params[:email])
      if @user
        redirect_to company_dashboard_path, notice: 'Email update successfully '
      end
    end

  end


  def get_user
    @user = Current.user
  end

end
