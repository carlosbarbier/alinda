class LoginController < ApplicationController
  def create; end

  def store
    user = User.find_by(email: params[:email])
    if user.present? && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to company_dashboard_path
    else
      flash.now[:alert] = 'Invalid email or password'
      render :create
    end
  end
end
