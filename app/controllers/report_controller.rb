class ReportController < ApplicationController
  before_action :require_user_logged_in!
  def index
    @company = Company.find(Current.user.company_id)
    @accidents = @company.accidents
  end

  def show
    @id = params[:id]
    @accident = Accident.where(id: Integer(params[:id])).preload(car: [:client]).first
    puts(@accident.car.client.inspect)
  end

  def register
    @accident = Accident.new
    @id = params[:id]
  end

  def store
    accident = params.permit(:date, :time, :description, :car_id,:user_id,:location)
    puts(accident)
    accident[:user_id] = Integer(accident[:user_id])
    accident[:car_id] = Integer(accident[:car_id])
    @accident = Accident.new(accident)
    if @accident.save
      redirect_to company_clients_url, notice: 'Report  has been registered successfully '
    else
      @id = params[:id]
      render :register
    end
  end

  def edit
    @id = params[:id]
    @accident = Accident.find(params[:id])
  end

  def update
    accident = params.permit(:date, :time, :description,:location)
    @accident = Accident.find(params[:id]).update(accident)
    if @accident
      redirect_to company_accidents_path, notice: 'Accident has been updated successfully'
    else
      @id = params[:id]
      @accident = Accident.find(params[:id])
      flash.now[:alert] = 'Something went wrong, please try again'
      render :edit
    end
  end

  def delete

    accident_delete = Accident.delete(params[:id])
    if accident_delete
      redirect_to company_accidents_path, notice: 'Accident has been deleted successfully'
    else
      redirect_to company_accidents_path, alert: 'Something went wrong'
    end
  end
end
