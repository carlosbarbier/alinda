class ForgotPasswordController < ApplicationController

  def edit

  end

  def update
    if params[:email].empty? || !params[:email].to_s.match((/\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i))
      flash.now[:alert] = 'Provide a valid email address'
      render 'edit'
    else
      @user = User.find_by(email: params[:email])
      if @user
        @user.update_attribute(:token, SecureRandom.uuid)
        @user.send_forgot_password_email
        redirect_to employee_login_path, notice: 'An email has been sent to you to update your password'
      else
        flash.now[:alert] = 'Email address not found'
        render 'edit'
      end
    end
  end

  def change
    @token = params[:token]
  end

  def reset
    if params[:password].empty? || params[:password_confirmation].empty?
      flash.now[:alert] = 'Invalid inputs'
      @token
      render 'change'
    else
      @user = User.find_by(token: params[:token])
      puts(user_params[:token])
      if @user
        @user.password = params[:password]
        @user.password_confirmation = params[:password_confirmation]
        @user.token = nil
        @user.save
        redirect_to employee_login_path, notice: 'An email has been sent to you to update your password'
      else
        @token
        flash[:alert] = 'Invalid or expired token'
        render :change
      end
    end

  end

  private

  def user_params
    params.permit(:password, :password_confirmation)
  end

end
