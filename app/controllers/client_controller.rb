
class ClientController < ApplicationController
  before_action :require_user_logged_in!
  decorates_assigned :client

  def index
    @company = Company.find(Current.user.company_id)
    @clients = @company.clients
  end

  def show
    @client = Client.where(id: Integer(params[:id])).preload(cars: [:plan]).first.decorate
  end

  def create
    @client = Client.new
  end

  def store
    client = params.permit(:first_name, :last_name, :dob, :email, :address, :phone, :licence, :user_id)
    client[:user_id] = Integer(client[:user_id])
    @client = Client.new(client)
    if @client.save
      redirect_to company_clients_path, notice: 'Client account has been registered successfully '
    else
      render :create
    end
  end

  def edit; end

  def form
    @client = Client.new
    @plans = Plan.all
    @car = Car.new
  end

  def save
    client = params.permit(:first_name, :last_name, :dob, :email, :address, :phone, :licence, :user_id)
    puts(client)
    car = params.permit(:make, :year, :color, :fuel, :engine, :door, :tax, :transmission, :mileage, :user_id, :plan_id)
    client[:user_id] = Integer(client[:user_id])
    @client = Client.new(client)
    if @client.save
      car[:client_id] = @client.id
      car[:plan_id] = Integer(car[:plan_id])
      @car = Car.new(car)
      if @car.save
        @plans = Plan.all
        @car = Car.new
        render :form
      else
        redirect_to company_dashboard_path, alert: 'Client create successfully '
      end
    else
      render :form
    end

  end

  def update; end

  def delete; end

end
