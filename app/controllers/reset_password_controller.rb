class ResetPasswordController < ApplicationController
  before_action :require_user_logged_in!
  before_action :get_user, only: [:edit, :update]

  def edit; end

  def update
    if params[:user][:password].empty? || params[:user][:current_password].empty?
      @user.errors.add(:password, 'provide all the fields')
      render 'edit'
    elsif @user.update_attributes(user_params)
      redirect_to company_dashboard_path, notice: 'Password update successfully '
    else
      render 'edit'
    end
  end

    private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def get_user
    @user = Current.user
  end
end
