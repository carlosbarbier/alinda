require 'digest'

class CompanyController < ApplicationController
  def create
    @company = Company.new
    @countries= Countryy.get_countries
  end

  def store
    company = params.permit(:name, :email, :address, :phone, :country)
    @company = Company.new(company)
    @countries= Countryy.get_countries
    @company.code = Digest::SHA1.hexdigest(@company.email)[8..16]
    if @company.save
      CompanyMailer.with(company: @company).new_company_email.deliver_later
      redirect_to employee_login_path, notice: 'Account created, please check your email for the code'
    else
      render :create
    end
    end
  end
