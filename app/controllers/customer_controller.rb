class CustomerController < ApplicationController
  before_action :require_user_logged_in!
  def my
    if session[:user_id]
      @company = Company.find(Current.user.company_id)
      @client_total = @company.clients.count
      @new_client_total = @company.clients.recent
      @accident_total = @company.accidents.count
      @car_total = @company.cars.count
      @cars_id = @company.cars.select(:id)
      @subscription_total = Subscription.where(car_id: @cars_id ).count
      @cancellation_total = Subscription.where(id: @cars_id ).cancellation
      @active_total = Subscription.where(id: @cars_id ).active
      @car_last_24_crashed = @company.accidents.recent
      @subscription_last_24 = Subscription.where(id: @cars_id ).recent
    end
  end

end
