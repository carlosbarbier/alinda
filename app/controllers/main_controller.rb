class MainController < ApplicationController
  def index
  end

  def about
  end

  def contact
    @contact = Contact.new
  end

  def store_contact
    contact = params.permit(:name, :email, :subject, :message)
    @contact = Contact.new(contact)
    if @contact.save
      redirect_to contact_path, notice: 'Message saved successfully One of our team members will contact you soon '
    else
      render :contact
    end
  end

  def how
  end

end
