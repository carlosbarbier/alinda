class EmployeeController < ApplicationController

  def create
    @user = User.new
  end

  def store
    company = get_company(params['code'])
    if company
      if company.user.create(user_params)
        redirect_to employee_login_path, notice: 'Account employee successfully '
      else
        redirect_to employee_login_path, alert: 'Something went wrong ,Please try later'
      end
    else
      redirect_to employee_register_path, alert: 'Invalid employee code'
    end
  end


  private

  def get_company(code)
    Company.find_by_code(code)
  end

  def user_params
    params.permit(:name, :email, :password, :password_confirmation, :code)
  end
end
