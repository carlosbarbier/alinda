class CarController < ApplicationController
  before_action :require_user_logged_in!
  @logger = Rails.logger
  def index
    @plans = Plan.all
  end

  def show; end

  def register
    @id = params[:id]
    @plans = Plan.all
    @car = Car.new
  end

  def store
    car = params.permit(:make, :year, :color, :fuel, :engine, :door, :tax, :transmission, :mileage, :user_id, :plan_id, :client_id)
    card = params.permit(:number,:expiry,:cvc)
    car[:user_id] = Integer(car[:user_id])
    car[:client_id] = Integer(car[:client_id])
    car[:plan_id] = Integer(car[:plan_id]) unless car[:plan_id].nil? || car[:plan_id].blank?
    @car = Car.new(car)
    if @car.save
      token = StripeServices::GenerateToken.new(card)
      if token.get_token != {}
        customer = StripeServices::CreateCustomer.new( @car.client.email,token.get_token.id)
        if customer.get_customer != {}
          subscription = StripeServices::CustomerSubscription.new(customer.get_customer.id,params[:stripe_id])
          if subscription.get_subscription != {}
            Subscription.create(car_id:@car.id, stripe_id:subscription.get_subscription.id)
            redirect_to company_clients_path, notice: 'Client account has been registered successfully '
          else
            @car.delete
            @id = params[:id]
            @plans = Plan.all
            flash.now[:alert] = 'We have some problem with payment'
            render :register
          end
        else
          @car.delete
          @id = params[:id]
          @plans = Plan.all
          flash.now[:alert] = 'We have some problem with payment'
          render :register
        end
      else
        @car.delete
        @id = params[:id]
        @plans = Plan.all
        flash.now[:alert] = 'We have some problem with payment'
        render :register
      end
    else
      @id = params[:id]
      @plans = Plan.all
      flash.now[:alert] = 'We have some problem with payment'
      render :register
    end
  end

  def edit; end

  def update;
  end

  def delete;


    subscription = Subscription.find_by(car_id: params[:id])
    Car.find(params[:id]).destroy
    if subscription
      cancellation = StripeServices::CancelSubscription.new(subscription.stripe_id)
      if cancellation
        subscription.update_attribute(:is_cancelled, true)
        redirect_to company_dashboard_path, notice: 'Subscription deleted successfully'
      end
    end

  end
end
