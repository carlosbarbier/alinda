module StripeServices

  class CreateCustomer
    @logger = Rails.logger
    Stripe.api_key = ENV['STRIPE_SECRET_KEY']

    def initialize(email, payment_method)
      @customer = {}
      begin
        @customer = Stripe::Customer.create(
          { email: email,
            payment_method: payment_method ,
            invoice_settings: {
              default_payment_method: payment_method,
            },
          }
        )
      rescue Stripe::CardError => e
        @logger.error( e.message)
      end
    end

    def get_customer
      @customer
    end

  end

end
