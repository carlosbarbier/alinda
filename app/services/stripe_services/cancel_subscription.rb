module StripeServices

  class CancelSubscription
    attr_reader :subscription

    Stripe.api_key = ENV['STRIPE_SECRET_KEY']

    def initialize(stripe_id)
      @subscription = {}
      begin
        @subscription = Stripe::Subscription.update(stripe_id, { cancel_at_period_end: true})
      rescue Stripe::InvalidRequestError => e
        logger.error e.message
      end
    end

    def get_subscription_cancellation
      @subscription
    end

  end

end
