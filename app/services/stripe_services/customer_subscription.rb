module StripeServices

  class CustomerSubscription
    attr_reader :subscription

    Stripe.api_key = ENV['STRIPE_SECRET_KEY']

    def initialize(customer,stripe_id)
      @subscription = {}
      begin
        @subscription = Stripe::Subscription.create({ customer: customer, items: [{price: stripe_id},]})
      rescue Stripe::InvalidRequestError => e
        Rails.logger.error(e.message)
      end
    end

    def get_subscription
      @subscription
    end

  end

end
