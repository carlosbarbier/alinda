module StripeServices

  class GenerateToken
    Stripe.api_key = ENV['STRIPE_SECRET_KEY']

    def initialize(card)
      puts(card[:expiry].slice(0, 2))
      @token = {}
      unless card[:expiry].empty?
        begin
          @token = Stripe::PaymentMethod.create({
                                                  type: 'card',
                                                  card: {
                                                    number: card[:number],
                                                    exp_month: card[:expiry].slice(0, 2),
                                                    exp_year: Integer("20" + card[:expiry].slice(5, 7)),
                                                    cvc: card[:cvc],
                                                  }, })
        rescue Stripe::CardError => e
          puts(e.message)
        end
      end
    end

    def get_token
      @token
    end
  end

end
