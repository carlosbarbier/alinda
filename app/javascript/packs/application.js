
import "channels"
import dt from 'datatables'
import  '/node_modules/bulma-extensions/bulma-steps/dist/css/bulma-steps.min.css';
import bulmaSteps from "bulma-extensions/bulma-steps/src/js";
import $ from 'jquery'
window.jQuery = $;
global.jQuery = $;
const moment = require('moment');
let card = require("card");


$(document).ready(async function (){
    $('.from_now').each(function(i, span) {
        span.textContent=` - ${moment(span.textContent.trim().slice(0,15)).fromNow()}`
    });


    $(".cancellation").click(function(){
        const id=$(this).attr("data-id")
        $.ajax({
            type: "POST",
            url: "/company/delete",
            beforeSend(xhr,settings) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
            },
            data: {
                id,

            },
            success: function (json) {
                window.location.reload()
            }
        });
    });
    $('.delete').click(async function() {
        $(".flash").hide();
    });

    $('#example').DataTable();

    $( "#example_filter label input" )
        .addClass( "input is-normal searchable" )
        .css("margin-bottom", "7px")
        .css("border-raduis", "10px")

    $( "#example_length label select" )
        .addClass( "select is-small" )
        .css("margin-bottom", "7px")
        .css("border-raduis", "15px")

    $("#example").bind("DOMSubtreeModified", function() {

        if ($("td.dataTables_empty").length>0){
            $("#example_info").hide()
            $("#example_paginate").hide()
        }else{
            $("#example_info").show()
            $("#example_paginate").show()
        }
    });

    bulmaSteps.attach()
    card=new Card({
        form: '#card',
        container: '.card-wrapper',

        placeholders: {
            number: '**** **** **** ****',
            expiry: '**/****',
            cvc: '***'
        },
    })




});



