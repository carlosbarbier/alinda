class CompanyMailer < ApplicationMailer
  def new_company_email
    @company = params[:company]

    mail(to: @company.email, subject: "You got a new order!")
  end
end
