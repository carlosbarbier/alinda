class ApplicationMailer < ActionMailer::Base
  default from: ENV['APP_MAIL']
  layout 'mailer'
end
