class ClientDecorator < Draper::Decorator
  delegate_all

  def full_name
    "#{object.first_name} #{object.last_name}"
  end

  def get_age
    ending = Integer(object.dob.last(4))
    age = Date.today.year - ending
    "#{age}"
  end

end
