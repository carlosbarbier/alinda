class Plan < ApplicationRecord
  validates :name, presence: true
  validates :description, presence: true
  validates :price, presence: true
  validates :stripe_id, presence: true
  has_many :cars
end
