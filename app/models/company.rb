class Company < ApplicationRecord
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze
  VALID_PHONE_REGEX = /[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.freeze
  validates :email, format: { with: VALID_EMAIL_REGEX }, presence: true,uniqueness: { case_sensitive: false }
  validates :name, presence: true
  validates :phone, presence: true, uniqueness: { case_sensitive: false },format: { with: VALID_PHONE_REGEX, multiline: true  }
  validates :address, presence: true
  validates :country, presence: true
  has_many :user
  has_many :clients, through: :user
  has_many :cars, through: :user
  has_many :accidents, through: :user
end
