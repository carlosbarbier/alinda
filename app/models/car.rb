class Car < ApplicationRecord
  validates :make, presence: true
  validates :year, presence: true
  validates :color, presence: true
  validates :fuel, presence: true
  validates :engine, presence: true
  validates :door, presence: true
  validates :tax, presence: true
  validates :transmission, presence: true
  validates :mileage, presence: true
  attribute :user_id
  attribute :plan_id
  attribute :client_id
  belongs_to :user
  belongs_to :plan
  belongs_to :client
  has_one :subscription
  scope :recent, ->(since = 1.day.ago) { where(created_at: since..).count }
end
