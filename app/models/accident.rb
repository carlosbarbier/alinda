class Accident < ApplicationRecord
  VALID_TIME = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/.freeze
  VALID_DATE = %r{^\d{2}/\d{2}/\d{4}$}.freeze
  validates :date, presence: true,format: { with: VALID_DATE, multiline: true  }
  validates :time, presence: true,format: { with: VALID_TIME, multiline: true  }
  validates :location, presence: true
  validates :description, presence: true
  attribute :user_id
  attribute :car_id
  belongs_to :user
  belongs_to :car

  scope :recent, ->(since = 1.day.ago) { where(created_at: since..).count }
end
