class User < ApplicationRecord

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze
  validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false },allow_blank: false
  validates :name, presence: true
  validates :code, presence: true
  validates :password, length: { minimum: 6 }
  attribute :company_id
  has_secure_password
  belongs_to :company, autosave: true
  has_many :client
  has_many :cars
  has_many :accidents


  def send_forgot_password_email
    ForgotPasswordMailer.forgot_password(self).deliver_now
  end

end
