class Subscription < ApplicationRecord
  validates :stripe_id, presence: true
  belongs_to :car
  scope :recent, ->(since = 1.day.ago) { where(created_at: since..).count }
  scope :cancellation, -> { where(is_cancelled: true).count }
  scope :active, -> { where(is_cancelled: false ).count }
end
