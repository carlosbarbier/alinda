class Client < ApplicationRecord
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze
  VALID_PHONE_REGEX = /[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.freeze
  VALID_DOB_REGEX = /^((?:0[0-9])|(?:[1-2][0-9])|(?:3[0-1]))\/((?:0[1-9])|(?:1[0-2]))\/((?:19|20)\d{2})$/.freeze
  validates :email, format: { with: VALID_EMAIL_REGEX }, presence: true, uniqueness: { case_sensitive: false }
  validates :first_name, presence: true
  validates :phone, presence: true,format: { with: VALID_PHONE_REGEX, multiline: true  }
  validates :dob, presence: true,format: { with: VALID_DOB_REGEX, multiline: true  }
  validates :address, presence: true
  validates :last_name, presence: true
  validates :licence, presence: true
  attribute :user_id
  has_many :cars

  scope :recent, ->(since = 1.day.ago) { where(created_at: since..).count }
end
