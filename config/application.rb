require_relative 'boot'

require 'rails/all'
Bundler.require(*Rails.groups)

module Alinda
  class Application < Rails::Application
    config.load_defaults 6.0
    config.generators.helper = false
    Bundler.require(*Rails.groups)
    Dotenv::Railtie.load
    HOSTNAME = ENV['HOSTNAME']
    config.autoload_paths << Rails.root.join('app/services')
  end
end
