
Rails.application.routes.draw do

  root to: 'main#index'
  get 'about', to: 'main#about'
  get 'contact', to: 'main#contact'
  get 'how-it-works', to: 'main#how'
  post 'contact', to: 'main#store_contact'

  get 'employee/register', to: 'employee#create'
  post 'employee/register', to: 'employee#store'
  get 'employee/login', to: 'login#create'
  post 'employee/login', to: 'login#store'
  get '/company/employee/email', to: 'update_email#edit'
  post '/company/employee/email', to: 'update_email#update'
  get '/company/employee/password', to: 'reset_password#edit'
  patch '/company/employee/password', to: 'reset_password#update'
  delete 'logout', to:'logout#destroy'
  get '/employee/forgot-password', to:'forgot_password#edit'
  post '/employee/forgot-password', to:'forgot_password#update'
  get 'reset/password', to:'forgot_password#change'
  post 'reset/password', to:'forgot_password#reset'

  get 'company/register', to: 'company#create'
  post 'company/register', to: 'company#store'
  get 'company/dashboard', to: 'customer#my'
  get 'company/clients/:id/show', to: 'client#show'
  get 'company/clients', to: 'client#index'
  get 'company/clients/register', to: 'client#create'
  post 'company/clients/register', to: 'client#store'
  get 'company/clients/:id/car/register', to: 'car#register'
  post 'company/clients/:id/car/register', to: 'car#store'
  post '/company/delete', to: 'car#delete'

  get 'company/car/:id/report/create', to: 'report#register'
  post 'company/car/:id/report/create', to: 'report#store'
  get 'company/accidents', to: 'report#index'
  post 'company/accidents/:id/delete', to: 'report#delete'
  get 'company/accidents/:id/show', to: 'report#show'
  get 'company/accidents/:id/edit', to: 'report#edit'
  post 'company/accidents/:id/edit', to: 'report#update'


end
